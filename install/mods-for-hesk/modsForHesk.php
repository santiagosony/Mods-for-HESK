<?php
define('IN_SCRIPT', 1);
define('HESK_PATH', '../../');
require(HESK_PATH . 'install/install_functions.inc.php');
require(HESK_PATH . 'hesk_settings.inc.php');

hesk_dbConnect();
?>
<html>
<head>
    <title>Mods For HESK <?php echo MODS_FOR_HESK_NEW_VERSION; ?> Install / Upgrade</title>
    <link href="../../hesk_style.css?<?php echo HESK_NEW_VERSION; ?>" type="text/css" rel="stylesheet"/>
    <link href="<?php echo HESK_PATH; ?>css/bootstrap.css?v=<?php echo $hesk_settings['hesk_version']; ?>"
          type="text/css" rel="stylesheet"/>
    <link href="<?php echo HESK_PATH; ?>css/bootstrap-theme.css?v=<?php echo $hesk_settings['hesk_version']; ?>"
          type="text/css" rel="stylesheet"/>
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="../../css/hesk_newStyle.css" type="text/css" rel="stylesheet"/>
    <script src="<?php echo HESK_PATH; ?>js/jquery-1.10.2.min.js"></script>
    <script language="Javascript" type="text/javascript" src="<?php echo HESK_PATH; ?>js/bootstrap.min.js"></script>
    <script language="Javascript" type="text/javascript"
            src="<?php echo HESK_PATH; ?>js/modsForHesk-javascript.js"></script>
    <script language="JavaScript" type="text/javascript"
            src="<?php echo HESK_PATH; ?>install/mods-for-hesk/js/ui-scripts.js"></script>
    <script language="JavaScript" type="text/javascript"
            src="<?php echo HESK_PATH; ?>install/mods-for-hesk/js/version-scripts.js"></script>
    <script language="JavaScript" type="text/javascript"
            src="<?php echo HESK_PATH; ?>js/bootstrap-datepicker.js"></script>
</head>
<body>
<div class="headersm">Mods for HESK <?php echo MODS_FOR_HESK_NEW_VERSION; ?> Install / Upgrade</div>
<div class="container">
    <div class="page-header">
        <h1>Mods for HESK <?php echo MODS_FOR_HESK_NEW_VERSION; ?> Install / Upgrade</h1>
    </div>
    <?php
    $allowInstallation = true;
    ?>
    <div class="row">
        <div class="col-md-5 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Database/File Requirements</div>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th colspan="2">Database Information / File Permissions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Database Host:</td>
                        <td><?php echo $hesk_settings['db_host']; ?></td>
                    </tr>
                    <tr>
                        <td>Database Name:</td>
                        <td><?php echo $hesk_settings['db_name']; ?></td>
                    </tr>
                    <tr>
                        <td>Database User:</td>
                        <td><?php echo $hesk_settings['db_user']; ?></td>
                    </tr>
                    <tr>
                        <td>Database Password:</td>
                        <td><?php echo $hesk_settings['db_pass']; ?></td>
                    </tr>
                    <tr>
                        <td>Database Prefix:</td>
                        <td><?php echo $hesk_settings['db_pfix']; ?></td>
                    </tr>
                    <tr>
                        <td>CREATE, ALTER, DROP Permissions:</td>
                        <td class="warning"><i class="fa fa-exclamation-triangle"></i> Please check before continuing!*
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            * Mods for HESK is unable to check database permissions automatically.
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <?php
        $tableSql = hesk_dbQuery('SHOW TABLES LIKE \'' . hesk_dbEscape($hesk_settings['db_pfix']) . 'settings\'');
        $version = NULL;
        $disableAllExcept = NULL;
        if (hesk_dbNumRows($tableSql) > 0) {
            $versionRS = hesk_dbQuery('SELECT `Value` FROM `' . hesk_dbEscape($hesk_settings['db_pfix']) . 'settings` WHERE `Key` = \'modsForHeskVersion\'');
            $versionArray = hesk_dbFetchAssoc($versionRS);
            $version = $versionArray['Value'];
        }
        ?>
        <div class="col-md-7 col-sm-12">
            <div class="panel panel-default">
                <div class="panel-heading">Install / Upgrade</div>
                <div class="panel-body">
                    <?php if ($allowInstallation) {
                        $prereqDiv = 'none';
                        $installDiv = 'block';
                    } else {
                        $prereqDiv = 'block';
                        $installDiv = 'none';
                    }
                    ?>
                    <div class="prereqsFailedDiv" style="display:<?php echo $prereqDiv; ?>">
                        <div class="alert alert-danger">
                            <p><i class="fa fa-times-circle"></i> You cannot install/upgrade Mods for HESK until the
                                requirements on the left have been met.</p>

                            <p><a href="modsForHesk.php" class="btn btn-default">Refresh</a></p>
                        </div>
                    </div>
                    <div class="installDiv" style="display:<?php echo $installDiv; ?>">
                        <div class="alert alert-info">
                            <p><i class="fa fa-exclamation-triangle"></i> Make sure that you have updated / installed
                                HESK first; otherwise installation will <b>fail</b>!</p>
                        </div>
                        <p>What version of Mods for HESK do you currently have installed?</p>
                        <hr>
                        <?php
                            if ($version != NULL && $version != MODS_FOR_HESK_NEW_VERSION) {
                                echo '<div class="row">';
                                echo '<div class="col-sm-12">';
                                echo '<p id="updateText">Mods for HESK has detected that you currently have version ' . $version . ' installed.
                                        The button you should click to upgrade has been highlighted for you. However, if
                                        Mods for HESK selected the wrong version, click <a href="javascript:void(0)" onclick="enableAllDisablable();">here</a> to reset them.</p>';
                                echo '</div>';
                                echo '</div>';
                            }
                        ?>
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.5.4') {
                                    $v254btn = 'btn-success';
                                    $disableAllExcept = '254';
                                } else {
                                    $v254btn = 'btn-default';
                                }
                                ?>
                                <a id="254" class="btn <?php echo $v254btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=25">2.5.4</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.5.3') {
                                    $v253btn = 'btn-success';
                                    $disableAllExcept = '253';
                                } else {
                                    $v253btn = 'btn-default';
                                }
                                ?>
                                <a id="253" class="btn <?php echo $v253btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=24">2.5.3</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.5.2') {
                                    $v252btn = 'btn-success';
                                    $disableAllExcept = '252';
                                } else {
                                    $v252btn = 'btn-default';
                                }
                                ?>
                                <a id="252" class="btn <?php echo $v252btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=23">2.5.2</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.5.1') {
                                    $v251btn = 'btn-success';
                                    $disableAllExcept = '251';
                                } else {
                                    $v251btn = 'btn-default';
                                }
                                ?>
                                <a id="251" class="btn <?php echo $v251btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=22">2.5.1</a>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.5.0') {
                                    $v250btn = 'btn-success';
                                    $disableAllExcept = '250';
                                } else {
                                    $v250btn = 'btn-default';
                                }
                                ?>
                                <a id="250" class="btn <?php echo $v250btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=21">2.5.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.4.2') {
                                    $v242btn = 'btn-success';
                                    $disableAllExcept = '242';
                                } else {
                                    $v242btn = 'btn-default';
                                }
                                ?>
                                <a id="242" class="btn <?php echo $v242btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=20">2.4.2</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.4.1') {
                                    $v241btn = 'btn-success';
                                    $disableAllExcept = '241';
                                } else {
                                    $v241btn = 'btn-default';
                                }
                                ?>
                                <a id="241" class="btn <?php echo $v241btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=19">2.4.1</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.4.0') {
                                    $v240btn = 'btn-success';
                                    $disableAllExcept = '240';
                                } else {
                                    $v240btn = 'btn-default';
                                }
                                ?>
                                <a id="240" class="btn <?php echo $v240btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=18">2.4.0</a>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.3.2') {
                                    $v232btn = 'btn-success';
                                    $disableAllExcept = '232';
                                } else {
                                    $v232btn = 'btn-default';
                                }
                                ?>
                                <a id="232" class="btn <?php echo $v232btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=17">2.3.2</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.3.1') {
                                    $v231btn = 'btn-success';
                                    $disableAllExcept = '231';
                                } else {
                                    $v231btn = 'btn-default';
                                }
                                ?>
                                <a id="231" class="btn <?php echo $v231btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=16">2.3.1</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.3.0') {
                                    $v230btn = 'btn-success';
                                    $disableAllExcept = '230';
                                } else {
                                    $v230btn = 'btn-default';
                                }
                                ?>
                                <a id="230" class="btn <?php echo $v230btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=15">2.3.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.2.1') {
                                    $v221btn = 'btn-success';
                                    $disableAllExcept = '221';
                                } else {
                                    $v221btn = 'btn-default';
                                }
                                ?>
                                <a id="221" class="btn <?php echo $v221btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=14">2.2.1</a>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.2.0') {
                                    $v220btn = 'btn-success';
                                    $disableAllExcept = '220';
                                } else {
                                    $v220btn = 'btn-default';
                                }
                                ?>
                                <a id="220" class="btn <?php echo $v220btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=13">2.2.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.1.1') {
                                    $v211btn = 'btn-success';
                                    $disableAllExcept = '211';
                                } else {
                                    $v211btn = 'btn-default';
                                }
                                ?>
                                <a id="211" class="btn <?php echo $v211btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=12">2.1.1</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.1.0') {
                                    $v210btn = 'btn-success';
                                    $disableAllExcept = '210';
                                } else {
                                    $v210btn = 'btn-default';
                                }
                                ?>
                                <a id="210" class="btn <?php echo $v210btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=11">2.1.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.0.1') {
                                    $v201btn = 'btn-success';
                                    $disableAllExcept = '201';
                                } else {
                                    $v201btn = 'btn-default';
                                }
                                ?>
                                <a id="201" class="btn <?php echo $v201btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=10">2.0.1</a>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '2.0.0') {
                                    $v200btn = 'btn-success';
                                    $disableAllExcept = '200';
                                } else {
                                    $v200btn = 'btn-default';
                                }
                                ?>
                                <a id="200" class="btn <?php echo $v200btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=9">2.0.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '1.7.0') {
                                    $v170btn = 'btn-success';
                                    $disableAllExcept = '170';
                                } else {
                                    $v170btn = 'btn-default';
                                }
                                ?>
                                <a id="170" class="btn <?php echo $v170btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=8">1.7.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <?php
                                if ($version == '1.6.1') {
                                    $v161btn = 'btn-success';
                                    $disableAllExcept = '161';
                                } else {
                                    $v161btn = 'btn-default';
                                }
                                ?>
                                <a id="161" class="btn <?php echo $v161btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=7">1.6.1</a>
                            </div>
                            <div class="col-md-3 col-sm-12">

                                <?php
                                if ($version == '1.6.0') {
                                    $v160btn = 'btn-success';
                                    $disableAllExcept = '160';
                                } else {
                                    $v160btn = 'btn-default';
                                }
                                ?>
                                <a id="160" class="btn <?php echo $v160btn; ?> btn-block disablable"
                                   href="installModsForHesk.php?v=6">1.6.0</a>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <a id="150" class="btn btn-default btn-block disablable"
                                   href="installModsForHesk.php?v=5">1.5.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <a id="141" class="btn btn-default btn-block disablable"
                                   href="installModsForHesk.php?v=4">1.4.1</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <a id="140" class="btn btn-default btn-block disablable"
                                   href="installModsForHesk.php?v=3">1.4.0</a>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <a id="130" class="btn btn-default btn-block disablable"
                                   href="installModsForHesk.php?v=2">1.3.0</a>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <a id="124" class="btn btn-default btn-block disablable"
                                   href="installModsForHesk.php?v=1">1.2.4</a>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="btn-group-vertical" role="group" style="width: 100%">
                                    <a class="btn btn-primary btn-block disablable" href="installModsForHesk.php?v=0">No
                                        previous installation</a>
                                    <button type="button" class="btn btn-danger btn-block" data-toggle="modal"
                                            data-target="#uninstallModal"><i class="fa fa-trash"></i> Uninstall Mods for
                                        HESK
                                    </button>
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-sm-12">
                                By proceeding, you agree to the terms of the <a
                                    href="http://opensource.org/licenses/MIT" target="_blank">MIT License.</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="uninstallModal" tabindex="-1" role="dialog" aria-labelledby="uninstallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="uninstallModalTitle"><i class="fa fa-trash"></i> Uninstall Mods for HESK
                </h4>
            </div>
            <div class="modal-body">
                <p>Are you sure you want to uninstall Mods for HESK?</p>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success" href="uninstallModsForHesk.php"><i class="fa fa-check"></i> Yes</a>
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> No
                </button>
            </div>
        </div>
    </div>
</div>
<?php
if ($disableAllExcept !== NULL) {
    echo '<script>disableAllDisablable(\'' . $disableAllExcept . '\')</script>';
}
?>
</body>
</html>
